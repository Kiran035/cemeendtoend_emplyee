package com.allstate.di;

import org.springframework.stereotype.Component;

@Component
public class Cat implements Pet{

    @Override
    public void feed() {
       System.out.println("Feed the cat");
    }
}
