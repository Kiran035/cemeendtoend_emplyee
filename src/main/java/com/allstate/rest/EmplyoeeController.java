package com.allstate.rest;

import com.allstate.entities.Employee;
import com.allstate.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmplyoeeController {
    @Autowired
    EmployeeService service;

    @RequestMapping(value="/status", method = RequestMethod.GET)
    public String getStatus()
    {
        return "Employee Rest API is running";
    }

    @RequestMapping(value="/total", method = RequestMethod.GET)
    public long getTotal()
    {
        return service.total();
    }

    @RequestMapping(value="/all", method = RequestMethod.GET)
    public List<Employee> all()
    {

        return service.all();
    }

    @RequestMapping(value="/find/{id}", method = RequestMethod.GET)
    public Employee getById(@PathVariable("id") int id)
    {
        return service.findById(id);
    }

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public void save(@RequestBody Employee emp)
    {
        service.save(emp);
    }

    @RequestMapping(value="/update", method = RequestMethod.PUT)
    public long update(@RequestBody Employee emp)
    {
        return service.update(emp);
    }
}
