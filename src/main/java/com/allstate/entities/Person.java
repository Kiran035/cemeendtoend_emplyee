package com.allstate.entities;

import org.springframework.data.annotation.Id;

public abstract  class Person {
   @Id
   private int id;
   private String name;
   private int age;
   private static int ageLimit =100;

   //default constructor
    public Person()
    {

    }

   public int getId()
   {
       return this.id;
   }

   public void setId(int id)
   {
       this.id = id;
   }

   public String getName()
   {
       return this.name;
   }

   public void setName(String name)
   {
       this.name = name;
   }

   public int getAge()
   {
       return this.age;
   }

   public void setAge(int age)
   {
       this.age = age;
   }

   public void display()
   {
       System.out.printf("values are %s, %s, %s",this.name, this.id,this.age);
       System.out.println();
   }

   public Person(int id, String name, int age) {
       this.id = id;
       this.name = name;
       this.age = age;
   }

   public static int getAgeLimit() {
       return ageLimit;
   }

   protected void protectedMethod()
   {
        System.out.println("Protected");
   }

   public void publicMethod()
   {
        System.out.println("public");
   }

    void defaultMethod()
   {
        System.out.println("default");
   }

  
}
